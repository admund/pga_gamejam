local T = display.newGroup()

T.callback = nil

------------------------------ 
local function onMove( event ) 
    local phase = event.phase 
    local target = event.target 
    
    if(phase == "began") then 
        
        display.getCurrentStage():setFocus(target) 
        print("began")
        
    elseif(phase == "moved") then 
        
        target.y = event.y 
        if(target.y < 22 ) then
            target.y = 22 
            else if(target.y + target.height > 320 + 22) then 
                target.y = 320 - target.height + 22                 
            end
        end
        
        local where = target.y / 160 - 1
        
        if(T.callback) then
            pcall(T.callback, where)
        end
        
    elseif(phase == "ended") then 
        
        display.getCurrentStage():setFocus(nil) 
        print("ended")
        
    end
    
    return true 
end

local sliderBack = display.newRect(T, display.contentWidth - 45 , 0,45,display.contentHeight) 
sliderBack:setFillColor(200, 200, 0, 250)

local slider = display.newRect(T, display.contentWidth - 45,display.contentHeight/2 - 22,45,45) 
slider:setReferencePoint(display.CenterReferencePoint)
slider:setFillColor(200, 200, 200, 250)
slider:addEventListener("touch", onMove)

function T.reset()
   -- slider.x = display.contentWidth - 45
    slider.y = display.contentHeight/2 - 22
end

return T
