local bosFactory = require("judge")
local physics = require("physics")
physics.start()
physics.setGravity(0, 0)
--physics.setDrawMode("hybrid")
local playerFactory = require("player")
local starFactory = require("stars")
--local finishFactory = require("finish")
--local enemyFactory = require("zawodnik")

local T = {}

T.levels = {}
T.currentPlayer = nil

-------------------------------------
function T.loadAllLevels()
    
    local miske = require("levelData")
    
    local levels = {}
    
    -- dla kazdego levelu
    for key, val in pairs(miske.layers) do
        
        local singleLevel = {}
        
        singleLevel.properties = val.properties
        singleLevel.listOfObjects = {}
        singleLevel.name = val.name
        
        -- dla kazdego obiektu na planszy
        for key1, val1 in pairs(val.objects) do
            
            local obj = {}
            obj.gid = val1.gid
            obj.x = val1.x
            obj.y = val1.y
            obj.rotation = val1.rotation
            obj.properties = val1.properties
            singleLevel.listOfObjects[ #singleLevel.listOfObjects + 1 ] = obj         
            
        end
        
        levels[ #levels + 1 ] = singleLevel
    end
    
    T.levels = levels
end

---------------------------------------
function T.makeMap( levelID ) 
    
    local grupa = display.newGroup()
    
    -- dodaj sedziow
    local levelSingle = T.levels[ levelID ]         
    
    local A = nil
    
    for key, val in ipairs( levelSingle.listOfObjects ) do
        
        if( val.gid == 1 ) then
            A = bosFactory.newJudgeVertical(val.x, val.y)
            A:startMove()
            physics.addBody(A, "dynamic", { isSensor = true })
        elseif(val.gid == 2) then
            A = bosFactory.newJudgeHorizontal(val.x, val.y)
            A:startMove()
            physics.addBody(A, "dynamic", { isSensor = true })            
        elseif(val.gid == 3) then
            A = bosFactory.newJudgeRound(val.x, val.y)   
            A:startMove()
            physics.addBody(A, "dynamic", { isSensor = true })            
        elseif(val.gid == 4) then
            A = bosFactory.newJudgeDiagonal(val.x, val.y) 
            A:startMove()
            physics.addBody(A, "dynamic", { isSensor = true })            
        elseif(val.gid == 5) then
            A = bosFactory.newJudgeDiagonal(val.x, val.y)
            A.rotation = A.rotation + 90
            A:startMove()
            physics.addBody(A, "dynamic", { isSensor = true })
        elseif(val.gid == 7) then  
            A = starFactory.createStar(val.x, val.y)
        elseif(val.gid == 8) then    
            --  A = finishFactory.createFinish(val.x, val.y)
        elseif(val.gid == 10) then    
            if( grupa.currentPlayer == nil ) then
                A = playerFactory.createNew(val.x, val.y) 
                grupa.currentPlayer = A
            end
        elseif(val.typ == 9) then    
            --A = playerFactory.createPlayer(val.x, val.y)
        end
        
        if(A)then
            grupa:insert(A)
        end
        
    end
    
    return grupa
    
end

return T
