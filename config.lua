
local mm = system.getInfo( "model" )
local floor = math.floor

local wid = display.pixelWidth
local hei = display.pixelHeight

local wid2 = 0
local hei2 = 0
local alias = {}

-- iphone 3
if(wid < 400) then
    wid2 = wid
    hei2 = hei
    alias =         
    {
        ["@2x"] = 2.0, 
    }
    
    -- androidy na 480
elseif(wid >= 400 and wid < 500) then    
    wid2 = floor(wid / 1.5)
    hei2 = floor(hei / 1.5)
    alias =         
    {
        ["@2x"] = 1.5, 
    }
    
    -- klasyczna retina
elseif(wid >= 500 and wid < 650) then
    wid2 = floor(wid / 2)
    hei2 = floor(hei / 2)
    
    -- iphone5
    if(hei == 1136) then
        alias =         
        {
            ["-568h@2x"] = 2,
            ["@2x"] = 1.95,
        }
        -- inne
    else
        
        alias =         
        {           
            ["@2x"] = 2,
        }         
        
    end
    
    -- ipady i tablety
    
    -- ipado podobne rzeczy
elseif(wid >= 650 and wid < 850) then
    wid2 = floor(wid / 2)
    hei2 = floor(hei / 2)
    
    alias =         
    {
        ["-Portrait"] = 2.0,     
        ["@2x"] = 2.0,
    }
    
    -- duze ipady i tablety    
else
    wid2 = floor(wid / 4)
    hei2 = floor(hei / 4)
    alias =         
    {
         
        ["-Portrait"] = 2.0,
        ["-Portrait@2x"] = 3.95,
        ["@2x"] = 2.0,
        ["@4x"] = 3.95,
    }
end

application =
{
    content =
    {
        width = wid2,
        height = hei2,
        scale = "letterbox",
        fps = 60,
        antialias = false,
        xalign = "center",
        yalign = "center",
        audioPlayFrequency = 44100,
        
        imageSuffix =             
    {
         
        ["-Portrait"] = 2.0,
        ["-Portrait@2x"] = 3.95,
        ["@2x"] = 2.0,
        ["@4x"] = 3.95,
    }
        
    },
    
}
