local T = {}
T.playerHitCallback = nil

local function onCollision(event)
    
    local phase = event.phase
    local obj1 = event.object1
    local obj2 = event.object2
    
    if (  phase == "ended") then
        
        if( obj2.class == "B" and obj1.class == "G" ) then
            
            -- make a hit
            pcall(T.playerHitCallback,  obj1, obj2)
            
        end
        if( obj1.class == "B" and obj2.class == "G" ) then
            
            -- make a hit
            pcall(T.playerHitCallback, obj2, obj1)
            
        end
    end
end

Runtime:addEventListener("collision", onCollision)

return T
