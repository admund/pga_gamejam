local uiSlider = require("uiSlider")
local timerek = require("uiTimer")

local mission = require("levels")
mission.loadAllLevels()

local player = nil


local startPoint = nil
local lastPoint = nil

local currentLevel = nil

local grupa = display.newGroup()

---------------------
local function touchGlobal( event )
   -- print("gloabl touch")
    local phase = event.phase
    local target = event.target
    
    -- poczatek
    if( phase == "began" ) then
        
        player.nextPoint = 1        
        
        -- clear group        
        local mmmm = grupa.numChildren
        
        for k = 1, mmmm do
            local dupsko = grupa[k]
            
            if(dupsko ~= nil) then
                dupsko:removeSelf()
                dupsko = nil
            end               
        end
        
        -- move unit to start 
        player:relocateUnit( )
        
        grupa:removeSelf()
        grupa = nil
        
        grupa = display.newGroup()
        
        startPoint = { x = event.x, y = event.y }
        lastPoint = startPoint
        
        player.pathMade = {}
        player.pathItems = {}
        
        local obj  = display.newCircle( grupa, event.x, event.y, 2)
        player.pathMade[ #(player.pathMade) + 1 ] = obj        
        player.pathItems[ #(player.pathItems) + 1 ] = lastPoint
        obj.xScale = 0.001; obj.yScale = 0.001
        transition.to(obj, { xScale = 2, yScale = 2, time = 100  } )
        transition.to(obj, { xScale = 1, yScale = 1, delay = 100, time = 100  } )
        player:toFront()
        
    elseif(phase == "moved")then   
        
        local dist = math.sqrt( (lastPoint.x - event.x) * (lastPoint.x - event.x) + (lastPoint.y - event.y) * (lastPoint.y - event.y))
        
        -- odleglosc i predkosc
        local dist = math.sqrt( (lastPoint.x - event.x) * (lastPoint.x - event.x) + (lastPoint.y - event.y) * (lastPoint.y - event.y))
        local angle = math.atan2( -event.y + lastPoint.y , -event.x + lastPoint.x )         
                
        local dx, dy        
                
        if( #player.pathMade % 2 == 1) then
            angle = angle + 90
            dx = math.cos( angle ) * 4
            dy = math.sin( angle ) * 4
        else    
            angle = angle - 90
            dx = math.cos( angle ) * 4
            dy = math.sin( angle ) * 4
        end
        
        if(dist >= 8) then
            
            lastPoint = { x = event.x, y = event.y }            
            player.pathItems[ #(player.pathItems) + 1 ] = lastPoint            
            local obj  = display.newCircle( grupa, event.x + dx, event.y + dy, 2)
            player.pathMade[ #(player.pathMade) + 1 ] = obj
            obj.xScale = 0.001; obj.yScale = 0.001
            transition.to(obj, { xScale = 2, yScale = 2, time = 100  } )
            transition.to(obj, { xScale = 1, yScale = 1, delay = 100, time = 100  } )
            player:toFront()
            -- if( #player.pathMade )
            
        end
        
    elseif(phase == "ended") then
        
    end
    
    return true
end

local label = display.newRect(0, 0, 480 - 45, 320)
label:setFillColor(0, 0)
label:addEventListener("touch", touchGlobal)

local gui = display.newGroup()
local resetBtn = display.newRect(0, 300, 55, 20)
resetBtn:setFillColor(150, 0, 0, 255)
local resetText = display.newText("RESET", 0, 300, 55, 20, "Arial", 15)
resetText:setTextColor(0, 0, 0, 255)
gui:insert(1, resetBtn)
gui:insert(2, resetText)

local function onReset(event)
    print("reset")
    local phase = event.phase
    if(phase == "ended") then
        Runtime:removeEventListener("enterFrame", player.enterFrame )
        currentLevel:removeSelf()
        currentLevel = nil
        currentLevel = mission.makeMap( 5 )
        player = currentLevel.currentPlayer
        Runtime:addEventListener( "enterFrame", player.enterFrame )
        uiSlider.callback = player.changeSpeed
        uiSlider.reset()
    end
    return true
end

gui:addEventListener("touch", onReset)

---------------------------------
currentLevel = mission.makeMap( 5 )
player = currentLevel.currentPlayer
print(mission, currentLevel, player)

-----------------------------------
Runtime:addEventListener( "enterFrame", player.enterFrame )
--Runtime:addEventListener( "touch", touchGlobal)

-----------------------------------
uiSlider.callback = player.changeSpeed

-----------------------------------
timerek.setupTimer( 30 )
timerek.startTimer()
timerek.callback = onGameIsOver

