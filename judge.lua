local T = {}

function T.newJudgeBasic(x, y, r) 
    
    r = r or 16
    
    local judge = display.newGroup()
    --judge.img = display.newGroup()
    judge:insert(1, display.newCircle(0, 0, r))
    judge:insert(2, display.newLine(0, 0, 100, 50))
    judge:insert(3, display.newLine(100, 50, 100, -50))
    judge:insert(3, display.newLine(0, 0, 100, -50))
    --judge:setReferencePoint(display.CenterReferencePoint)
    judge.xOrigin = 0
    judge.yOrigin = 0
    
    judge.x = x
    judge.y = y
    
    judge.moveSpeed = 0.1
    judge.isSensor = true
    judge.isEnable = false
    
    judge.name = "basic"
    
    function judge:setStartPos(x, y)
        self.startX = x
        self.startY = y
    end
    
    function judge:setStopPos(x, y)
        self.stopX = x
        self.stopY = y
    end
    
    function judge:startWigle()
        self.rotation = -15
        self:wigle1()
    end
    
    function judge:wigle1()
        transition.to(self, { rotation = self.startAngle + self.rotation + 30, time = 1500, onComplete = self.wigle2})
    end
    
    function judge:wigle2()
        transition.to(self, { rotation = self.startAngle + self.rotation - 30, time = 1500, onComplete = self.wigle1})
    end
    
    return judge
end

function T.newJudgeHorizontal(x, y, r) 
    local judge = T.newJudgeBasic(x, y, r)
    judge.startAngle = 0
    
    judge.name = "horizontal"
    
    function judge:startMove()
        --print("startMove")
        transition.to(self, { x = self.stopX, time = (math.abs(self.startX - self.stopX))/self.moveSpeed, onComplete = self.rotate})
    end
    
    function judge:rotate()
        --print("rotate")
        --self.startAngle = 180
        transition.to(self, { rotation = self.rotation + 180, time = 500, onComplete = self.back})
    end
    
    function judge:back()
        --print("back")
        transition.to(self, { x = self.startX, time = (math.abs(self.startX - self.stopX))/self.moveSpeed, onComplete = self.rotate2})
    end
    
    function judge:rotate2()
        --print("rotate2")
        --self.startAngle = 0
        transition.to(self, { rotation = self.rotation - 180, time = 500, onComplete = self.startMove})
    end
    
    return judge
end

function T.newJudgeVertical(x, y, r) 
    local judge = T.newJudgeBasic(x, y, r)
    judge.startAngle = 0
    
    judge.name = "vertical"
    
    function judge:startMove()
        --print("startMove")
        self.rotation = 90
        transition.to(self, { y = self.stopY, time = (math.abs(self.startY - self.stopY))/self.moveSpeed, onComplete = self.rotate})
    end
    
    function judge:rotate()
        --print("rotate")
        --self.startAngle = 180
        transition.to(self, { rotation = self.rotation + 180, time = 500, onComplete = self.back})
    end
    
    function judge:back()
        --print("back")
        transition.to(self, { y = self.startY, time = (math.abs(self.startY - self.stopY))/self.moveSpeed, onComplete = self.rotate2})
    end
    
    function judge:rotate2()
        --print("rotate2")
        --self.startAngle = 0
        transition.to(self, { rotation = self.rotation - 180, time = 500, onComplete = self.startMove})
    end
    
    return judge
end

function T.newJudgeDiagonal(x, y, r) 
    local judge = T.newJudgeBasic(x, y, r)
    judge.startAngle = 0
    
    judge.name = "diagonal"
    
    function judge:startMove()
        --print("startMove")
        self.rotation = 45
        transition.to(self, { y = self.stopY, time = (math.abs(self.startY - self.stopY))/self.moveSpeed, onComplete = self.rotate})
        transition.to(self, { x = self.stopX, time = (math.abs(self.startX - self.stopX))/self.moveSpeed})
    end
    
    function judge:rotate()
        --print("rotate")
        --self.startAngle = 180
        transition.to(self, { rotation = self.rotation + 180, time = 500, onComplete = self.back})
    end
    
    function judge:back()
        --print("back")
        
        transition.to(self, { y = self.startY, time = (math.abs(self.startY - self.stopY))/self.moveSpeed, onComplete = self.rotate2})
        transition.to(self, { x = self.startX, time = (math.abs(self.startX - self.stopX))/self.moveSpeed})
    end
    
    function judge:rotate2()
        --print("rotate2")
        --self.startAngle = 0
        transition.to(self, { rotation = self.rotation - 180, time = 500, onComplete = self.startMove})
    end
    
    return judge
end

function T.newJudgeRandom(x, y, r) 
    local judge = T.newJudgeBasic(x, y, r)
    
    judge.name = "random"
    
    function judge:startMove()
        --print("startMove")
        transition.to(self, { x = self.stopX, time = 2000, onComplete = self.randomRotate})
    end
    
    function judge:randomRotate()
        --print("randomMove")
        math.randomseed(os.time())
        local angle = math.random(0, 360);
        transition.to(self, { rotation = angle, time = 2000, onComplete = self.randomMove})
    end
    
    function judge:randomMove()
        --print("randomMove")
        local newX = 50 * math.sin(self.rotation)
        local newY = 50 * math.cos(self.rotation)
        --local newX = math.random(self.x, self.x + 50);
        ---local newY = math.random(self.y, self.y + 50);
        transition.to(self, { x = x + newX, time = 2000, onComplete = self.randomRotate})
        transition.to(self, { y = y + newY, time = 2000})--, onComplete = self.randomMove})
    end
    
    return judge
end

function T.newJudgeRound(x, y, r, RR)
    local judge = T.newJudgeBasic(x, y, r)
    judge.name = "round"
    judge.xReference = RR
    judge.yReference = RR
    
    function judge:startMove()
        --print("startMove")
        transition.to(self, { rotation = 359, time = 5000, onComplete = self.restet})
    end
    
    function judge:restet()
        self.rotation = 0
        self:startMove()
    end
    
    return judge
end

return T

