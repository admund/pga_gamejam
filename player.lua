
local Player = {}

----------------------
function Player.createNew( x, y )
    
    local T = display.newRect(30,30,25,25)
    T.startX = x
    T.startY = y
    T.x = x
    T.y = y
    T.speed = 1
    T.pathMade = {}
    T.pathItems = {}
    T.nextPoint = 1
    T.startDone = false
    
    ---------------------
    function T.relocateUnit()
        
        local function doItAfter( who )
            who.x = who.startX
            who.y = who.startY
            transition.to( who, { xScale = 1, yScale = 1, time = 100 } )
        end    
        transition.to( T, { xScale = 0.0001, yScale = 0.0001, time = 100, onComplete = doItAfter } )
        
    end
    
    ---------------------
    function T.moveUnit()
        
        local nextField
        
        if(T.speed > 0) then
            nextField = T.pathMade[ T.nextPoint ]
        else
            nextField = T.pathMade[ T.nextPoint - 2 ]
        end 
        
        if (nextField) then
            
            -- odleglosc i predkosc
            local dist = math.sqrt( (nextField.x - T.x) * (nextField.x - T.x) + (nextField.y - T.y) * (nextField.y - T.y))
            local angle = math.atan2( -T.y + nextField.y , -T.x + nextField.x )         
            local dx = math.cos( angle ) * math.abs(T.speed)
            local dy = math.sin( angle ) * math.abs(T.speed)
            
            T.rotation = math.deg( angle )
            T:translate(dx, dy)
            
            if( dist <= 2) then  
                
                if(T.speed > 0) then
                    local RR = T.pathMade[ T.nextPoint ]
                    if(RR) then
                        RR.xScale = 2
                        RR.yScale = 2
                        RR:setFillColor(240, 120, 120)
                    end
                    T.nextPoint = T.nextPoint + 1; 
                else
                    local RR = T.pathMade[ T.nextPoint ]
                    if(RR)then
                        RR.xScale = 1
                        RR.yScale = 1
                        RR:setFillColor(240, 240, 240)
                    end
                    T.nextPoint = T.nextPoint - 1; 
                end
            end
        end    
    end
    
    ---------------------
    function T.enterFrame( event )
        
        local phase = event.phase
        local target = event.target
                
        print(T, T.x)
        
        --if (T.startDone) then
        T.moveUnit()
        --end
        
        return true
        
    end
    
    ---------------------
    function T.changeSpeed( dx )
        
        if(dx > 0) then    
            dx = dx * dx
        else
            dx = - dx * dx
        end
        
        if(dx > 1) then
            dx = 1
        elseif( dx < -1) then
            dx = -1
        end
        
        dx = dx * 3
        
        T.speed = dx
        
    end
    
    return T
    
end

return Player

