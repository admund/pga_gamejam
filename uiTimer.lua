local T = display.newGroup()

T.value = 1000
T.textUI = display.newText( T, "text", 240, 10, "Arial", 20)
T.callback = nil

------------------------------
local function onListener()    
    if(T.callback)then
        pcall( T.callback, "Game over" )
    end    
end

------------------------------
local function onTimer(  )
    T.value = T.value - 1
    T.textUI.text = T.value .. " sec"
    
    if(T.value <= 0) then
        native.showAlert("Game over", "No more time", { "Ok" }, onListener)
        T.stopTimer()
    end
    
end
T.timer = nil

------------------------------
function T.stopTimer()
    timer.pause( T.timer )
end

------------------------------
function T.setupTimer( tim )
    T.value = tim
end

------------------------------
function T.resumeTimer()
    timer.resume( T.timer )
end

------------------------------
function T.resetTimer( )
    T.value = 100
end

------------------------------
function T.startTimer( )
    T.value = 100
    if(T.timer)then
        T.cancel(T.timer)
    end
    T.timer = timer.performWithDelay(1000, onTimer, -1)
end

return T

