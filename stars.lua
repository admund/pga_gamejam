local T = {}


function T.createStar(x, y)
    
    --local star = display.newRect(x, y, 10, 10)
    local star = display.newCircle(x,y,5)
    star.name = "star"
    star:setFillColor(255, 255, 0, 255)
    
    function star:shineAnim1()
        self.rotation = 0
        transition.to(self, { rotation = 359, delay = 2000, time = 1000})
        transition.to(self, { xScale = 1.2, delay = 2000, time = 500})
        transition.to(self, { yScale = 1.2, delay = 2000, time = 500, onComplete = self.shineAnim2})
    end
    
    function star:shineAnim2()
        transition.to(self, { xScale = 1, time = 500})
        transition.to(self, { yScale = 1, time = 500, onComplete = self.shineAnim1})
    end
    
    function star:killStar()
        transition.to(self, { rotation = 359, delay = 2000, time = 1000})
        transition.to(self, { xScale = 1.5, delay = 2000, time = 500})
        transition.to(self, { yScale = 1.5, delay = 2000, time = 500, onComplete = self.deadAnim2})
    end

    function star:deadAnim2()
        transition.to(self, { xScale = 0.0001, delay = 2000, time = 500})
        transition.to(self, { yScale = 0.0001, delay = 2000, time = 500, onComplete = self.reamove})
    end
    
    function star:reamove()
        self:removeSelf()
        self = nil
    end
        
    return star
end


return T



