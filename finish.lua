local T = {}
T.starCatchCallback = nil
T.endCallback = nil

function T.createFinish(x, y)
    local finish = display.newRect(x, y, 10, 40)
    finish.name = "finish"
    finish:setFillColor(0, 255, 255, 255)
    
    function finish:pickStar(starTab)
        if(#starTab == 0) then
            if(self.endCallback) then
                pcall(self.endCallback)
            end
        end
    end
    
    return finish
end

return T
